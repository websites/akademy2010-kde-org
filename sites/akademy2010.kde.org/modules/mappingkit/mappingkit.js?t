// $Id: mappingkit.js,v 1.1.2.3 2008/05/26 19:29:11 openwereld Exp $

(function() {
  /**
   * Namespace: MappingKit
   */
  window.MappingKit = {
  };
})();

/**
 * Version number.
 */
MappingKit.VERSION_NUMBER = "$Revision: 1.1.2.3 $";

MappingKit.settings = {};

/**
Function: $defined
	Returns true if the passed in value/Object is defined, that means it is not null or undefined.

Arguments:
	o - the Object to inspect.
	
Returns:
	{boolean}
*/

function $defined(o){
	return (o != undefined && o != null);
};


/**
Function: $type
	Returns the type of Object that matches the element passed in.

Arguments:
	obj - the Object to inspect.
	
Example:
	>var myString = 'hello';
	>$type(myString); //returns "string"

Returns:
	'element' - if o is a DOM element node
	'textnode' - if o is a DOM text node
	'whitespace' - if o is a DOM whitespace node
	'arguments' - if o is an arguments object
	'array' - if o is an object
	'object' - if o is an object
	'string' - if o is a string
	'number' - if o is a number
	'boolean' - if o is a boolean
	'function' - if o is a function
	'regexp' - if o is a regular expression
	'date' - if o is a Date
	'class' - if o is a Class. (created with new Class, or the extend of another class).
	'collection' - if o is a native htmlelements collection, such as childNodes, getElementsByTagName .. etc.
	null - if the object is not defined or none of the above.
*/

function $type(o){
	if (! $defined(o)) {return null;}
	if (o.htmlElement) {return 'element';}
	
	var type = typeof o;
	
	if (type == 'object' && o.nodeName) {
		switch (o.nodeType) {
			case 1: return 'element';
			case 3: return (/\S/).test(o.nodeValue) ? 'textnode' : 'whitespace';
		}
	}
	
	if (type == 'object' || type == 'function') {
		switch (o.constructor) {
			case Array: return 'array';
			case RegExp: return 'regexp';
			case Class: return 'class';
			case Date: return 'date';
			// add additional Object types that you care about here
		}
		
		if (typeof o.length == 'number') {
			if (o.item) {return 'collection';}
			if (o.callee) {return 'arguments';}
		}
	}
	
	return type;
};


/**
Function: isType
	Returns true if the Object has the same type as supplied.

Arguments:
	o - the Object to inspect.
	type - the String name for type
	
Returns:
	{boolean}
*/

function isType(o, type) {
	return type == $type(o);
}

